package edu.javacourse.xml;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class SaxExample {

  public static void main(String args[]) {

    // Имя файла
    final String fileName = "Phonebook.xml";

    try {
      SAXParserFactory factory = SAXParserFactory.newInstance();
      SAXParser saxParser = factory.newSAXParser();

      // Здесь мы определили анонимный класс, расширяющий класс DefaultHandler
      DefaultHandler handler = new DefaultHandler() {
        // Поле для указания, что тэг NAME начался
        private final Stack<Level> levels = new Stack<>();

        @Override
        public void startElement (String uri, String localName,
                                  String qName, Attributes attributes)
            throws SAXException
        {
          levels.push(new Level(qName));
        }

        @Override
        public void characters(char ch[], int start, int length) throws SAXException {
          levels.get(levels.size() - 1).add(new String(ch, start, length));
        }

        @Override
        public void endElement (String uri, String localName, String qName)
            throws SAXException
        {
          Level current = levels.get(levels.size() - 1);
          if (!current.getNodeName().equals(qName))
            throw new SAXException("Bad xml format");

          Level level = levels.pop();
          if ("NAME".equals(qName))
            System.out.println("Name: " + level.getResult());
        }
      };

      // Стартуем разбор методом parse, которому передаем наследника от DefaultHandler, который будет вызываться в нужные моменты
      saxParser.parse(fileName, handler);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  private static class Level {
    String nodeName;
    List<String> texts = new LinkedList<>();

    public Level(String nodeName) {
      this.nodeName = nodeName;
    }

    public void add(String it) {
      if (it == null || it.trim().equals("")) return;
      texts.add(it.trim());
    }

    public String getNodeName() {
      return nodeName;
    }

    public String getResult() {
      return String.join(" ", texts);
    }
  }

}

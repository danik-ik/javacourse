package edu.javacourse.counter;

public class CounterTester
{
  public static void main(String[] args) throws InterruptedException {
    Counter counter = new Counter();
    for(int i=0; i<200; i++) {
      CounterThread ct = new CounterThread(counter);
      ct.start();
    }
    Thread.sleep(1000);

    System.out.println("Counters:");
    for (long l : counter.getCounters()) {
      System.out.println(l); 
    }
  }
}

class Counter
{
  private long counter1 = 0L;
  private long counter2 = 0L;
  private long counter3 = 0L;

  public void increaseCounter1() {
    counter1++;
  }

  public synchronized void increaseCounter2() {
    counter2++;
  }

  public void increaseCounter3() {
    synchronized(this) {
      counter3++;
    }
  }

  public long[] getCounters() {
    return new long[] {counter1, counter2, counter3};
  }
}

class CounterThread extends Thread
{
  private Counter counter;

  public CounterThread(Counter counter) {
    this.counter = counter;
  }

  @Override
  public void run() {
    for(int i=0; i<1000; i++) {
      counter.increaseCounter1();
      counter.increaseCounter2();
      counter.increaseCounter3();
    }
  }
}
/*
 * Класс для демонстрации рефлексии
 */
package edu.javacourse.reflection;

public class SimpleClass
{
  private String first;

  public String getFirst() {
    return first;
  }

  public String simple() {
    return "Method1";
  }

  public String concat(String s1, String s2) {
    return s1 + s2;
  }

  public long x3(long a) {
    return a * 3;
  }

  public static long x5(long a) {
    return a * 5;
  }
}
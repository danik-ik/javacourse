/*
 * Класс для создания экземпляра нужного класса
 */
package edu.javacourse.reflection;

import java.lang.annotation.Annotation;
import java.util.PropertyResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

@FinanceAnnotation(financeBuilder = "edu.javacourse.reflection.WebFinanceInfoBuilder")
public class FinanceInfoBuilderFactory
{
  // Сделаем констуанту для имения нужного нам свойства
  private static final String BUILDER_CLASS = "builder.class";
  
  private static String getBuilderClassFromResource() {
    PropertyResourceBundle pr = (PropertyResourceBundle)
        PropertyResourceBundle.getBundle("edu.javacourse.reflection.builder");
    return pr.getString(BUILDER_CLASS);
  }
  
  private static String getBuilderClassFromAnnotation() {
    Annotation ann =
        FinanceInfoBuilderFactory.class.getAnnotation(FinanceAnnotation.class);
    FinanceAnnotation fa = (FinanceAnnotation)ann;
    return fa.financeBuilder();
  }
  
  private static String getBuilderClass() {
    return getBuilderClassFromResource();
  }

  public static FinanceInfoBuilder getFinanceInfoBuilder() {
    // Получаем имя класса из файла builder.properties
    String className = getBuilderClass();

    try {
      // Загружаем класс по имени
      Class cl = Class.forName(className);
      // Т.к. наш класс должен имплементировать интерфейс FinanceInfoBuilder
      // то мы можем сделать приведение к интерфейсу
      FinanceInfoBuilder builder = (FinanceInfoBuilder)cl.newInstance();
      return builder;
    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
      ex.printStackTrace(System.out);
    }
    return null;
  }
}
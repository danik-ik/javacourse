public class SimpleThreadManager
{
  public static void main(String[] arg) {
    for(int i=0; i<10; i++) {
      new SimpleThread().start();
      new Thread(new SimpleRunnable()).start();
    }
  }
}

class SimpleThread extends Thread
{
  @Override
  public void run() {
    try {
      long pause = Math.round(Math.random()*2000);
      Thread.sleep(pause);
      System.out.println("Simple Thread - pause="+pause);
    } catch(InterruptedException i_ex) {

    }
  }
}

class SimpleRunnable implements Runnable
{
  @Override
  public void run() {
    try {
      long pause = Math.round(Math.random()*2000);
      Thread.sleep(pause);
      System.out.println("Simple Runnable - pause="+pause);
    } catch(InterruptedException i_ex) {

    }
  }
}
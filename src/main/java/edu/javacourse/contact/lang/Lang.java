package edu.javacourse.contact.lang;

import java.util.PropertyResourceBundle;

public class Lang {

  public static String getString(String key) {
    return ResourceStrings.getResourceString(key); 
  }
}

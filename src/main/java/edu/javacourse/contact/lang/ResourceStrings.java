package edu.javacourse.contact.lang;

import java.util.Locale;
import java.util.PropertyResourceBundle;

public class ResourceStrings {
  private static ResourceStrings ourInstance = new ResourceStrings();
  private static PropertyResourceBundle res;

  private ResourceStrings() {
    res = (PropertyResourceBundle)
        PropertyResourceBundle.getBundle("edu.javacourse.contact.strings");
  }
  
  public static String getResourceString(String key) {
    return res.getString(key);
  } 
}
